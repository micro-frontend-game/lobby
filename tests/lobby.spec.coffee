describe 'Lobby micro-component', ->
  before () -> Cypress.config 'includeShadowDom', true

  beforeEach ->
    cy.mockWS('ws://fake-domain.org/')
    cy.visit('/lobby')
    cy.window().then (@win) =>
      @doc = @win.document
      @doc.body.innerHTML = ''
    cy.runInIframeCtx """
      const modulesToPreload = [
        ["Lobby", "/lobby/lobby.mjs"],
        ["UserConfig", "/lobby/userConfig.mjs"]
      ]
      let loadComponent
      window.ws = new WebSocket('ws://fake-domain.org/')
      export default import(BASE_DOMAIN + "/loader/loader.mjs")
      .then(mod => loadComponent = mod.default )
      .then(() => {
        return Promise.all(modulesToPreload.map(([_, path]) =>
          import(BASE_DOMAIN + path)
          .then(mod => loadComponent(mod.default, { socket: window.ws }) )
        ))
      })
      .then(wrappedComponents => {
        modulesToPreload.forEach(([name], i)=> {
          window[name] = wrappedComponents[i]
          window[name].Component.forceOpenShadow = true
        })
      })
    """
    cy.waitForWSConn()

  it 'builds', ->
    @win.Lobby.create parent: @doc.body

  it 'renders rooms according to websocket message', ->
    @win.Lobby.create parent: @doc.body
    cy.mockSocketIOSrvPush('rooms', [
      { name: 'Minha Sala', id: 'aaa' },
      { name: 'Um Jogo', id: 'bbb' },
      { name: 'Um jogo com nome muito longo que não deve ser apresentado na tela', id: 'ccc' },
    ])
    cy.get('ul#rooms > li').should('have.length', 3)
    cy.contains('ul#rooms > li', 'Minha Sala')
    cy.contains('ul#rooms > li', 'Um Jogo')
    cy.contains('ul#rooms > li', 'Um jogo com nome muito longo que não deve ser apresentado na tela')

  it 'renders links according to room ids', ->
    @win.Lobby.create parent: @doc.body
    cy.mockSocketIOSrvPush('rooms', [
      { name: 'Minha Sala', id: 'aaa' },
      { name: 'Um Jogo', id: 'bbb' },
      { name: 'Um jogo com nome muito longo que não deve ser apresentado na tela', id: 'ccc' },
    ])
    cy.contains('a[href$="#/game/aaa"]', 'Minha Sala')
    cy.contains('a[href$="#/game/bbb"]', 'Um Jogo')
    cy.contains('a[href$="#/game/ccc"]', 'Um jogo com nome muito longo que não deve ser apresentado na tela')

  it 'creates a public room', ->
    @win.Lobby.create parent: @doc.body
    cy.get('#create-room input[name="name"]').type('Nova Sala')
    cy.get('#create-room input[type="submit"]').click()
    cy.afterWSMessage(/"create-room",\{"name":"Nova Sala","isPrivate":false\}/)
    cy.mockSocketIOSrvPush('rooms', [
      { id: 'aaa', name: 'Nova Sala', isPrivate: false }
    ])
    cy.contains('#rooms a[href$="#/game/aaa"]', 'Nova Sala')

  it 'creates a private room', ->
    @win.Lobby.create parent: @doc.body
    cy.get('#create-room input[name="name"]').type('Nova Sala')
    cy.get('#create-room input[name="is-private"]').click()
    cy.get('#create-room input[type="submit"]').click()
    cy.afterWSMessage(/"create-room",\{"name":"Nova Sala","isPrivate":true\}/)
    cy.mockSocketIOSrvPush('private-rooms', [
      { id: 'aaa', name: 'Nova Sala', isPrivate: true }
    ])
    cy.contains('#private-rooms a[href$="#/game/aaa"]', 'Nova Sala')
