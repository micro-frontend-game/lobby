import './cy-mock-socket.ts'

Cypress.Commands.add('runInIframeCtx', (code) =>
  cy.document({ log: false }).then(doc =>
    new Promise((resolve, reject) => {
      const resolveId = '_' + Math.random().toString(32).split('.')[1]
      const rejectId = '_' + Math.random().toString(32).split('.')[1]
      ;(doc as any)[resolveId] = resolve
      ;(doc as any)[rejectId] = reject

      const script = doc.createElement('script')
      script.setAttribute('type', 'module')
      const BASE = doc.location.href
      const BASE_DOMAIN = BASE.replace(/(https?:\/\/[^\/]+)\/.*/, '$1')
      code = `
        const BASE = ${JSON.stringify(BASE)}
        const BASE_DOMAIN = ${JSON.stringify(BASE_DOMAIN)}
        ${code}
      `
      script.innerHTML = `
        import('data:text/javascript;base64,${btoa(code)}').then(mod => {
          if (mod.default instanceof Promise) {
            mod.default.then(document.${resolveId}).catch(document.${rejectId})
          } else {
            document.${rejectId}(Error('You must return a promise.'))
          }
        })
      `
      doc.head.appendChild(script)
    })
  )
)
