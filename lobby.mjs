"use strict";

import BaseComponent from '../base-component/BaseComponent.mjs'

const userConfigModURL = import.meta.url.replace(/(.*)\/.*/, '$1/userConfig.mjs')

export default class Lobby extends BaseComponent {

  template = `
    div#lobby
      h1 text="Microfrontend Memory"
      @load(${userConfigModURL})#user-config
      form#create-room
        div
          input name="name" type="text" placeholder="Room name"
          label
            input name="is-private" type="checkbox"
            @text Private room
        input type="submit" value="Create room"
      h2 text="Public Rooms"
      ul#rooms.room-list
      h2 text="Your Private Rooms"
      ul#private-rooms.room-list
  `

  style = `
    :host * {
      box-sizing: border-box;
    }
    #lobby {
      font-family: sans-serif;
      font-size: 16px;
    }
    h1 {
      font-size: 30px;
      text-align: center;
      color: #999;
      margin: 30px;
    }
    #create-room {
      border: 1px solid #AAA;
      padding: 20px;
      margin-bottom: 10px;
    }
    #create-room, #create-room > div {
      display: flex;
      align-items: center;
      gap: 10px;
    }
    #user-config {
      position: absolute;
      top: 0;
      right: 0;
    }
    .room-list {
      display: grid;
      grid-template-columns: repeat(4, 1fr);
      gap: 20px;
      border: 1px solid #AAA;
      padding: 20px;
      margin: 0;
      list-style: none;
    }
    .room-list li {
      display: block;
      margin: 0;
      padding: 20px;
      border: 1px solid #AAA;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  `

  get win() {
    return window.Cypress ? window.innerWindow : window
  }

  constructor() {
    super()
  }

  __afterCreate() {

  }

  afterConnected() {
    if (!this.shared.socket) {
      throw Error('Socket not provided.')
    }

    this.root.getElementById('create-room').addEventListener('submit', event => {
      event.preventDefault()

      const form = event.target
      const name = form.querySelector('input[name="name"]').value
      const isPrivate = form.querySelector('input[name="is-private"]').checked

      this.createRoom(name, isPrivate)
    })

    this.shared.socket.addEventListener('message', message => {
      const [event, payload] = JSON.parse(message.data)
      if (event === 'rooms') {
        this.updateRooms(payload)
      }
      else if (event === 'private-rooms') {
        this.updatePrivateRooms(payload)
      }
    })
  }

  createRoom(name, isPrivate) {
    const message = ['create-room', {name, isPrivate}]
    this.shared.socket.send(JSON.stringify(message))
  }

  updateRooms(rooms) {
    const parent = this.root.querySelector('#rooms')
    while (parent.firstChild) parent.firstChild.remove()
    rooms.forEach(room => {
      const li = document.createElement('li')
      parent.appendChild(li)
      const link = document.createElement('a')
      li.appendChild(link)
      link.href = '#/game/' + room.id
      link.title = room.name
      link.append(room.name)
    })
  }

  updatePrivateRooms(rooms) {
    const parent = this.root.querySelector('#private-rooms')
    while (parent.firstChild) parent.firstChild.remove()
    rooms.forEach(room => {
      const li = document.createElement('li')
      parent.appendChild(li)
      const link = document.createElement('a')
      li.appendChild(link)
      link.href = '#/game/' + room.id
      link.title = room.name
      link.append(room.name)
    })
  }

  attributeChangedCallback(name, oldValue, newValue) {

  }

}
