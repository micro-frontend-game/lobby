describe 'User config micro-component', ->
  before () -> Cypress.config 'includeShadowDom', true

  beforeEach ->
    cy.mockWS('ws://fake-domain.org/')
    cy.visit('/lobby')
    cy.window().then (@win) =>
      @doc = @win.document
      @doc.body.innerHTML = ''
    cy.runInIframeCtx """
      const modulesToPreload = [
        ["Lobby", "/lobby/lobby.mjs"],
        ["UserConfig", "/lobby/userConfig.mjs"]
      ]
      let loadComponent
      window.ws = new WebSocket('ws://fake-domain.org/')
      export default import(BASE_DOMAIN + "/loader/loader.mjs")
      .then(mod => loadComponent = mod.default )
      .then(() => {
        return Promise.all(modulesToPreload.map(([_, path]) =>
          import(BASE_DOMAIN + path)
          .then(mod => loadComponent(mod.default, { socket: window.ws }) )
        ))
      })
      .then(wrappedComponents => {
        modulesToPreload.forEach(([name], i)=> {
          window[name] = wrappedComponents[i]
          window[name].Component.forceOpenShadow = true
        })
      })
    """
    cy.waitForWSConn()

  it 'shows username form on render', ->
    @win.Lobby.create parent: @doc.body
    cy.get 'form#create-user'
    cy.get 'form#create-user input[name="name"]'
    cy.get 'form#create-user input[type="submit"]'

  it 'shows user identity on render', ->
    @win.Lobby.create parent: @doc.body
    cy.get '#user-config' # Wait for DOM build.
    cy.wait 50 # let the async int to run.

    cy.mockSocketIOSrvPush 'user-data',
      name: 'Fulano', email: 'fulano@example.com'
    cy.get '#user-config #user-info img'
    cy.contains '#user-config #user-info', 'Fulano'
