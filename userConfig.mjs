"use strict";

import BaseComponent from '../base-component/BaseComponent.mjs'

export default class UserConfig extends BaseComponent {

  async template() {
    return `div#user-config` +
    (this.isEditing
    ? `
        form#create-user
          input name="name" type="text" placeholder="Your Name" value="${this.name || ''}"
          input name="email" type="text" placeholder="user@example.com" value="${this.email || ''}"
          input type="submit" value="Save"`
    : `
        div#user-info
          img src="${await this.libravatarURL()}"
          span text="${this.name || 'Não definido.'}"
      `
    )
  }

  style = ``

  isEditing = true
  name = null
  email = null

  async libravatarURL() {
    const encoded = new TextEncoder().encode(this.email)
    return crypto.subtle.digest('SHA-256', encoded)
    .then(buffer => {
      const sha256 = Array.from(new Uint8Array(buffer))
      .map(b => b.toString(16).padStart(2, '0')).join('')
      return `https://seccdn.libravatar.org/avatar/${sha256}?d=monsterid&s=64`
    })
  }

  afterConnected() {
    if (!this.shared.socket) {
      throw Error('Socket not provided.')
    }
    this.shared.socket.addEventListener('message', message => {
      const [event, payload] = JSON.parse(message.data)
      if (event === 'user-data') {
        this.name = payload.name
        this.email = payload.email
        this.isEditing = false
        this.redraw()
      }
    })
  }
}
