/// <reference types="cypress" />

import { WebSocket, Server } from 'mock-socket'

let mockSrvWS: Server|null = null
let srvSocket: WebSocket|null = null
let srvSocketReceivedHistory: string[] = []

declare namespace Cypress {
  interface cy {
    mockWS(wsSrvURL: string, onMessage: (socket: WebSocket, data: string)=>void): void;
    srvSideSocket(): WebSocket;
    mockWSSrvPush(data: string): void;
    mockSocketIOSrvPush(event: string, payload: any): void;
    waitForWSConn(url?: string, opts?: TWaiterOpts): Promise<WebSocket>;
    waitForWSMessage(matcher: RegExp, opts?: TWaiterOpts): Promise<WebSocket>;
  }
}

type TOnMessageHandler = (socket: WebSocket, data: string)=>void
const defaultOnMessageHandler = (socket: WebSocket, data: string): void => void(0)

declare global {
  interface Window {
    WebSocket: typeof WebSocket
  }
}

/* Will mock a WS server before the page load. */
Cypress.Commands.add('mockWS', (wsSrvURL: string, onMessage: TOnMessageHandler = defaultOnMessageHandler): void => {
  cy.on("window:before:load", (window: Window) => {
    window.WebSocket = WebSocket
    mockSrvWS = new Server(wsSrvURL)
    mockSrvWS.on('connection', (socket: WebSocket) => {
      srvSocket = socket
      srvSocket.on('message', msg => {
        srvSocketReceivedHistory.push(msg.toString())
        onMessage(srvSocket as WebSocket, msg.toString())
      })
    })
  })
  cy.on("window:unload", () => {
    if (mockSrvWS) {
      mockSrvWS.close()
      mockSrvWS.stop()
    }
    mockSrvWS = null
    srvSocket = null
  })
})

Cypress.Commands.add('srvSideSocket', (): Promise<WebSocket> =>
  new Promise((resolve, reject)=> {
    const tryer = ()=> {
      if (srvSocket) resolve(srvSocket)
      else setTimeout(tryer, 100)
    }
    tryer()
  })
)

Cypress.Commands.add('srvSideSocketClose', (): void => {
  if (srvSocket) srvSocket.close()
})

function mockWSSrvPush(data: string): void {
  if (srvSocket) srvSocket.send(data)
  else throw Error('Did you miss cy.mockWS(url)?')
}

Cypress.Commands.add('mockWSSrvPush', mockWSSrvPush)

Cypress.Commands.add('mockSocketIOSrvPush', (event: string, payload: any): void => {
  mockWSSrvPush(JSON.stringify([ event, payload ]))
})

type SrvOrSocket = WebSocket | Server //& EventTarget //& { on: (ev:string, handler:(arg:any)=>void)=>void }
type EventName = "error" | "open" | "message" | "close"

function mkWaiter(target: SrvOrSocket, eventName: EventName, handler: (arg:any)=>void|boolean, timeout?: number): Promise<WebSocket> {
  return new Promise((resolve, reject) => {
    const timeoutId = setTimeout(
      () => finish(Error('Timeout exceeded')),
      timeout || 10000
    )
    const testHandler = (arg: any) => handler(arg) && finish()
    target.on(eventName, testHandler)
    const finish = (err?: Error): void => {
      clearTimeout(timeoutId)
      target.removeEventListener(eventName, testHandler)
      if (err) reject(err)
      else resolve(srvSocket as WebSocket)
    }
  })
}

type TWaiterOpts = { timeout?: number, log?: boolean }

Cypress.Commands.add('waitForWSConn', (url?: string, opts: TWaiterOpts={}): Cypress.Chainable<WebSocket> => {
  const start = Date.now()
  opts = { timeout: 3000, log: true, ...opts }
  if (opts.log) Cypress.log({
    name: 'waitForWSConn',
    displayName: 'Wait for WS Connection'
  })
  return cy.wrap(new Promise((resolve, reject)=> {
    function waitForWSConn() {
      if (!url) url = (mockSrvWS as Server & {url: string}).url
      const handler = (socket: WebSocket): void|true => {
        if ((!url) || (socket.url === url)) return true
      }
      if (srvSocket && srvSocket.url === url) return resolve(srvSocket)
      resolve(mkWaiter(mockSrvWS, 'connection' as EventName, handler, opts.timeout))
    }
    function waitForMockSrvWS() {
      if (mockSrvWS) return waitForWSConn()
      if ((Date.now()-start) >= opts.timeout)
        reject(Error('Did you run **`cy.mockWS("ws://some/url")`** before?'))
      else
        setTimeout(waitForMockSrvWS, 20)
    }
    waitForMockSrvWS()
  }), {log: false, timeout: opts.timeout+200})
})

// Try to match old messages, if don't, call waitForWSMessage()
Cypress.Commands.add('afterWSMessage', (matcher: RegExp, opts: TWaiterOpts={}): Promise<WebSocket> => {
  opts = { log: true, ...opts }
  if (opts.log) Cypress.log({
    name: 'afterWSMessage',
    displayName: 'After WS Msg',
    message: matcher.toString().replace(/^\/(.*)\/[a-z]*$/, '$1')
  })
  const CY = cy as unknown as Cypress.cy // forcing to get declarations
  const preload = srvSocket
                ? Promise.resolve()
                : CY.waitForWSConn('', { log: false })
  return (preload as Promise<any>).then(()=> {
    const found = srvSocketReceivedHistory.find( (msg: string)=> matcher.test(msg) )
    if (found) return Promise.resolve(srvSocket as WebSocket)
    else return waitForWSMessage(matcher, opts)
  })
})

function waitForWSMessage(matcher: RegExp, opts: TWaiterOpts={}): Promise<WebSocket> {
  const CY = cy as unknown as Cypress.cy // forcing to get declarations
  const preload = srvSocket
                ? Promise.resolve()
                : CY.waitForWSConn('', { log: false })
  return (preload as Promise<any>).then(()=> {
    const handler = (data: string): void|true => {
      if (matcher.test(data)) return true
    }
    return mkWaiter(srvSocket as WebSocket, 'message', handler, opts.timeout)
  })
}

// Try to match new messages.
Cypress.Commands.add('waitForWSMessage', (matcher: RegExp, opts: TWaiterOpts={}): Promise<WebSocket> => {
  opts = { timeout: 4000, log: true, ...opts }
  if (opts.log) Cypress.log({
    name: 'waitForWSMessage',
    displayName: 'Wait WS Msg',
    message: matcher.toString().replace(/^\/(.*)\/[a-z]*$/, '$1')
  })
  return waitForWSMessage(matcher, opts)
})
